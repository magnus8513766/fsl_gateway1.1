const express = require('express');
const engine =require('ejs-mate');
const path= require('path');
const morgan = require('morgan');
const passport=require('passport');
const session=require('express-session');
const flash =require('connect-flash');

//inicializaciones
const app = express();
require('./database');
require('./passport/local-auth');


//configuraciones
app.set('views',path.join(__dirname,'views'));
app.engine('ejs',engine);
app.set('view engine','ejs');
app.set('port', process.env.PORT || 3000);

//middlewares
app.use(morgan('dev')); //ver  que metodo estamos reciviendo
app.use(express.urlencoded({extended: false}));//recivir datos en forma de objeto o datos
app.use(session({
    secret: 'mysecret',
    resave:false,
    saveUninitialized: false
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

app.use((req,res,next)=>{
    app.locals.signupMessage=req.flash('signupMessage');
    app.locals.signinMessage=req.flash('signinMessage');
    app.locals.user = req.user;
    //console.log(app.locals);
    next();
});

//rutas
app.use('/', require('./routes/index'));

//inicio de servidor
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
})
