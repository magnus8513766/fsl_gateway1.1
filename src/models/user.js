const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const {Schema} = mongoose;

const userSchema = new Schema({
    email: String,
    password: String
});
//sifrado password
userSchema.methods.encryptpassword= (password)=>{
    return bcrypt.hashSync(password,bcrypt.genSaltSync(10));
};
//comparacion de cifrado
userSchema.methods.validarpassword= function (password){
    return bcrypt.compareSync(password, this.password);
};
module.exports=mongoose.model('users',userSchema);