const passport = require('passport');
const localstrategy = require('passport-local').Strategy;

const User=require ('../models/user');
//guatda informascion en navegador
passport.serializeUser((user,done)=>{
    done(null,user.id);
});

passport.deserializeUser(async(id,done)=>{
    const user =await User.findById(id)
    done(null,user);
});

passport.use('local-signup', new localstrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, email, password, done) => {
    
    const user = await User.findOne({email:email});
    if(user){
        return done(null,false,req.flash('signupMessage','el email ya existe'));
    }else{
        const user1=new User();
        user1.email= email;
        user1.password=user1.encryptpassword(password);
        await user1.save();
        done(null,user1);
    }
}));


passport.use('local-signin', new localstrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  }, async (req, email, password, done) => {
    const user = await User.findOne({email: email});
    if(!user) {
      return done(null, false, req.flash('signinMessage', 'usuario no encontrado'));
    }
    if(!user.validarpassword(password)) {
      return done(null, false, req.flash('signinMessage', 'Incorrect Password'));
    }
    return done(null, user);
  }));
